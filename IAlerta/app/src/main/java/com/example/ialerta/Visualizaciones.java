package com.example.ialerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Visualizaciones extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizaciones);
    }
    public void irMenu(View view)
    {
        Intent Inicio=new Intent(Visualizaciones.this,CrearAlerta.class);
        startActivity(Inicio);
    }
}