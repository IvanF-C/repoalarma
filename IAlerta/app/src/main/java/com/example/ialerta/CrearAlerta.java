package com.example.ialerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrearAlerta extends AppCompatActivity {
    public String usuarioIdAlerta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_alerta);


        Button btnCrearAlerta=(Button)findViewById(R.id.buttonCrearAlerta);
        btnCrearAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ServicioPeticion servicio= API.getApi(CrearAlerta.this).create(ServicioPeticion.class);
                //                Call<DatosCrearAlerta> datosCrearAlertaCall=servicio.crearAlerta();
                //                datosCrearAlertaCall.enqueue(new Callback<DatosCrearAlerta>() {
                //                    @Override
                //                    public void onResponse(Call<DatosCrearAlerta> call, Response<DatosCrearAlerta> response) {
                //                        DatosCrearAlerta datosCrearAlerta=response.body();
                //                        if(response.body()==null)
                //                        {
                //                            Toast.makeText(CrearAlerta.this,"Llene los campos",Toast.LENGTH_LONG).show();
                //                            return;
                //                        }
                //                        if (datosCrearAlerta.estado=="true")
                //                        {
                //                            usuarioIdAlerta=datosCrearAlerta.usuarioid;
                //                            Toast.makeText(CrearAlerta.this,"Alerta Creada " + usuarioIdAlerta, Toast.LENGTH_LONG ).show();
                //
                //                        }
                //                        else
                //                        {
                //                            Toast.makeText(CrearAlerta.this,"Se Presento un Error", Toast.LENGTH_LONG ).show();
                //                        }
                //                    }
                //
                //                    @Override
                //                    public void onFailure(Call<DatosCrearAlerta> call, Throwable t) {
                //
                //                    }
                //                });
            }
        });


    }

    public void irAlertas(View view)
    {
        Intent Inicio=new Intent(CrearAlerta.this,Alertas.class);
        startActivity(Inicio);
    }
    public void irAlertasUsuario(View view)
    {
        Intent Inicio=new Intent(CrearAlerta.this,AlertaUsuario.class);
        startActivity(Inicio);
    }
    public void irCrearVisualizacion(View view)
    {
        Intent Inicio=new Intent(CrearAlerta.this,CrearVisualizacion.class);
        startActivity(Inicio);
    }
    public void irVisualizacionUsuario(View view)
    {
        Intent Inicio=new Intent(CrearAlerta.this,VisualizacionUsuario.class);
        startActivity(Inicio);
    }
    public void irVisualizacion(View view)
    {
        Intent Inicio=new Intent(CrearAlerta.this,Visualizaciones.class);
        startActivity(Inicio);
    }

}