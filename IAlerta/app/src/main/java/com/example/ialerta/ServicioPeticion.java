package com.example.ialerta;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServicioPeticion {

    //Registro de usuario
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<DatosRegistro> datosRegistro(@Field("username") String correo, @Field("password") String contrasena);

    //Login de usuario
    @FormUrlEncoded
    @POST("api/login")
    Call<DatosLogin> datosLogin(@Field("username") String nombreUsuario,@Field("password") String contrasena);

    //Crear Alerta
    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<DatosCrearAlerta> crearAlerta(@Field("usuarioId") String usuarioId);

    //Alertas
    @POST("api/alertas")
    Call<DatosAlertas> alertas();

    //Alerta de Usuarios
    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<DatosAlertaUsuario> alertasUsuario(@Field("usuarioId") String usuarioId);

    //Crear Visualizacion
    @FormUrlEncoded
    @POST("api/crearvisualizacionalerta")
    Call<DatosCrearVisualizacion> crearVisualizacion(@Field("usuarioId") String usuarioId,@Field("alertaId") String alertaId);

    //Visualizaciones Usuario
    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<DatosVisualizacionUsuario> visualizacionUsuario(@Field("usuarioId") String usuarioId);

    //Visualizaciones
    @FormUrlEncoded
    @POST("api/visualizaciones")
    Call<DatosVisualizaciones> visualizaciones();


}
