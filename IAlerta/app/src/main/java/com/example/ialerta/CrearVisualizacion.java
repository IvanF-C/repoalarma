package com.example.ialerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CrearVisualizacion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_visualizacion);
    }
    public void irMenu(View view)
    {
        Intent Inicio=new Intent(CrearVisualizacion.this,CrearAlerta.class);
        startActivity(Inicio);
    }
}