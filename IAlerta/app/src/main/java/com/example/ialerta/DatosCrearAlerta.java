package com.example.ialerta;

import java.security.PublicKey;

public class DatosCrearAlerta {
    public String estado;
    public  String usuarioid;

    public DatosCrearAlerta(String usuarioid)
    {
        this.usuarioid=usuarioid;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(String usuarioid) {
        this.usuarioid = usuarioid;
    }
}
