package com.example.ialerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public String usuarioL;
    public String password;
    public String ApiToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final SharedPreferences prefe=getSharedPreferences(" Credenciales ",Context.MODE_PRIVATE);
        String token=prefe.getString(" Token", "");


        Button btnAcceder = (Button) findViewById(R.id.buttonAcceder);
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText etUsuario=(EditText)findViewById(R.id.editTextUsuario);
                EditText etContrasena=(EditText)findViewById(R.id.editTextContrasena);
                final String usuario=etUsuario.getText().toString();
                String password=etContrasena.getText().toString();
                if(TextUtils.isEmpty(usuario))
                {
                    etUsuario.setError("Ingrese el Usuario");
                    etUsuario.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(password))
                {
                    etContrasena.setError("Ingrese Contraseña");
                    etContrasena.requestFocus();
                    return;
                }


                ServicioPeticion servicio=API.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<DatosLogin> datosLoginCall=servicio.datosLogin(etUsuario.getText().toString(),etContrasena.getText().toString());
                datosLoginCall.enqueue(new Callback<DatosLogin>() {
                    @Override
                    public void onResponse(Call<DatosLogin> call, Response<DatosLogin> response) {
                        DatosLogin login=response.body();
                        if(response.body()==null)
                        {
                            Toast.makeText(MainActivity.this,"Error",Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (login.estado=="true")
                        {
                           ApiToken=login.token;
                           usuarioL=login.usuario;
                           GuardarPreferencias();
                           Toast.makeText(MainActivity.this,"Bienvenido: " + usuario, Toast.LENGTH_LONG ).show();
                            Intent IraPanel = new Intent(MainActivity.this,CrearAlerta.class);
                            IraPanel.putExtra("username",usuario);
                            startActivity(IraPanel);

                        }
                        else
                        {
                            Toast.makeText(MainActivity.this,"Se Presento un Error", Toast.LENGTH_LONG ).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DatosLogin> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Ocurrio un Error de Conexion", Toast.LENGTH_LONG ).show();
                    }
                });
            }

        });
    }

    public  void  GuardarPreferencias()
    {
        SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token=ApiToken;
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putString("Token :",token);
        editor.commit();

    }

    public void irRegistro(View view)
    {
        Intent Inicio=new Intent(MainActivity.this,Registro.class);
        startActivity(Inicio);
    }

    public void irCrearAlerta(View view)
    {
        Intent Inicio=new Intent(MainActivity.this,CrearAlerta.class);
        startActivity(Inicio);
    }
}
