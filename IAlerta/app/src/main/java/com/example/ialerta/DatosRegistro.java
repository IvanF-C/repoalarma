package com.example.ialerta;

public class DatosRegistro {
    public String usuario;
    public String password;
    public String estado;
    public String detalle;

    public DatosRegistro(String usuario,String password)
    {
        this.usuario=usuario;
        this.password=password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}
