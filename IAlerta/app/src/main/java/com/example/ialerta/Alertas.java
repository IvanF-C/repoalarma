package com.example.ialerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Alertas extends AppCompatActivity {
    ArrayList<String> ListaCompletaId=new ArrayList<>();
    ArrayList<String> ListaCompletausuarioId=new ArrayList<>();
    ArrayList<String> ListaCompletaCreatedAt=new ArrayList<>();

    public  ListView lvAlertas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas);

        lvAlertas=(ListView) findViewById(R.id.ListViewAlertas);


        final ArrayAdapter arrayAdapter= new ArrayAdapter(this,R.layout.activity_alertas,ListaCompletaId);

        ServicioPeticion service=API.getApi(Alertas.this).create(ServicioPeticion.class);
        Call<DatosAlertas> datosAlertasCall=service.alertas();
        datosAlertasCall.enqueue(new Callback<DatosAlertas>() {
            @Override
            public void onResponse(Call<DatosAlertas> call, Response<DatosAlertas> response) {
                DatosAlertas peticion=response.body();
                if(peticion.estado.equals("false"))
                {
                    Toast.makeText(Alertas.this,"Todo Bien",Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(Alertas.this,"Que mal un error",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DatosAlertas> call, Throwable t) {
                Toast.makeText(Alertas.this,"Ocurrio un Error 2",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void irMenu(View view)
    {
        Intent Inicio=new Intent(Alertas.this,CrearAlerta.class);
        startActivity(Inicio);
    }
}
