package com.example.ialerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {
    public String  usuarioR;
    public String  passwordR;
    public String  passwordRR;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Button btnRegistro;
        btnRegistro=(Button)findViewById(R.id.buttonRegistro);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText etUsuario= (EditText)findViewById(R.id.editTextUsuarioR);
                EditText etContrasenaR=(EditText)findViewById(R.id.editTextContrasenaR);
                EditText etContrasenaRR=(EditText)findViewById(R.id.editTextContrasenaRR);

                String usuarioR=etUsuario.getText().toString();
                String passwordR=etContrasenaR.getText().toString();
                String passwordRR=etContrasenaRR.getText().toString();

                if (TextUtils.isEmpty(usuarioR))
                {
                    etUsuario.setError("Porfavor Ingrese el Usuario");
                    etUsuario.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(passwordR))
                {
                    etContrasenaR.setError("Porfavor Ingrese la Contraseña");
                    etContrasenaR.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(passwordRR))
                {
                    etContrasenaRR.setError("Porfavor Repita la Contraseña");
                    etContrasenaRR.requestFocus();
                    return;
                }

                if (!passwordR.equals(passwordRR))
                {
                    Toast.makeText(Registro.this,"Las Contraseñas no Coinciden",Toast.LENGTH_LONG).show();
                }

                ServicioPeticion servicio = API.getApi(Registro.this).create(ServicioPeticion.class);
                Call<DatosRegistro> datosRegistroCall=servicio.datosRegistro(etUsuario.getText().toString(),etContrasenaR.getText().toString());
                datosRegistroCall.enqueue(new Callback<DatosRegistro>() {
                    @Override
                    public void onResponse(Call<DatosRegistro> call, Response<DatosRegistro> response) {
                        DatosRegistro peticion=response.body();
                        if (response.body()== null)
                        {
                            Toast.makeText(Registro.this,"Se presento un error",Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (peticion.estado=="true")
                        {
                            startActivity(new Intent(Registro.this,MainActivity.class));
                            Toast.makeText(Registro.this,"Registro Exitoso",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(Registro.this,"El Error fue: " + peticion.detalle,Toast.LENGTH_LONG);
                        }

                    }

                    @Override
                    public void onFailure(Call<DatosRegistro> call, Throwable t) {
                        Toast.makeText(Registro.this,"Se presento un Error",Toast.LENGTH_LONG);

                    }
                });


            }
        });

    }

    public void irLogin(View view)
    {
        Intent Inicio=new Intent(Registro.this,MainActivity.class);
        startActivity(Inicio);
    }
}
