package com.example.ialerta;

import java.util.ArrayList;

public class DatosAlertas {
    public String estado;
    public ArrayList<ArregloAlertas> alertas;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<ArregloAlertas> getAlertas() {
        return alertas;
    }

    public void setAlertas(ArrayList<ArregloAlertas> alertas) {
        this.alertas = alertas;
    }
}
