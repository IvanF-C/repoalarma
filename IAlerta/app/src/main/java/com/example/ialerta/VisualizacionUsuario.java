package com.example.ialerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class VisualizacionUsuario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizacion_usuario);
    }
    public void irMenu(View view)
    {
        Intent Inicio=new Intent(VisualizacionUsuario.this,CrearAlerta.class);
        startActivity(Inicio);
    }
}